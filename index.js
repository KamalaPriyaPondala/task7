const users = [
    {
        uid: 001,
        email: 'john@dev.com',
        personalInfo: {
            name: 'John',
            address: {
                line1: 'westwish st',
                line2: 'washmasher',
                city: 'wallas',
                state: 'WX'
            }
        }
    },
    {
        uid: 063,
        email: 'a.abken@larobe.edu.au',
        personalInfo: {
            name: 'amin',
            address: {
                line1: 'Heidelberg',
                line2: '',
                city: 'Melbourne',
                state: 'VIC'
            }
        }
    },
    {
        uid: 045,
        email: 'Linda.Paterson@gmail.com',
        personalInfo: {
            name: 'Linda',
            address: {
                line1: 'Cherry st',
                line2: 'Kangaroo Point',
                city: 'Brisbane',
                state: 'QLD'
            }
        }
    }
]

//task5
function returnUsers(users) {
    var output = []

    for (i = 0; i < users.length; i++) {
        var a = users[i].personalInfo.name
        var b = users[i].email
        var c = users[i].personalInfo.address.state
        output.push({ a, b, c });
    }
    return output;
}


//task6
let btnGet = document.querySelector('.users');
let btnsort = document.querySelector('.sort');
let myTable = document.querySelector('#table');

let headers = ['Name', 'email', 'state']

btnGet.addEventListener('click', () => {
    let table = document.createElement('table')
    let headerRow = document.createElement('tr')

    headers.forEach(headerText => {
        let header = document.createElement('th');
        let textNode = document.createTextNode(headerText);

        header.appendChild(textNode);
        headerRow.appendChild(header);
    });

    table.appendChild(headerRow);

    let x = returnUsers(users)

    x.forEach(user => {
        let row = document.createElement('tr');

        Object.values(user).forEach(text => {
            let cell = document.createElement('td');
            let textNode = document.createTextNode(text);

            cell.appendChild(textNode);
            row.appendChild(cell);
        })
        table.appendChild(row);
    });

    myTable.appendChild(table);

});


//task7

let sorting = returnUsers(users)
sorting.sort(function (y, z) {
    if (y.a.toLowerCase() < z.a.toLowerCase()) {
        return -1;
    }

    else if (y.a.toLowerCase() > z.a.toLowerCase()) {
        return 1;
    }
    return 0;
});

btnsort.addEventListener('click', () => {
    let table = document.createElement('table')
    let headerRow = document.createElement('tr')

    headers.forEach(headerText => {
        let header = document.createElement('th');
        let textNode = document.createTextNode(headerText);

        header.appendChild(textNode);
        headerRow.appendChild(header);
    });

    table.appendChild(headerRow);

    sorting.forEach(user => {
        let row = document.createElement('tr');

        Object.values(user).forEach(text => {
            let cell = document.createElement('td');
            let textNode = document.createTextNode(text);

            cell.appendChild(textNode);
            row.appendChild(cell);
        })
        table.appendChild(row);
    });

    myTable.appendChild(table);

});











